<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'auto_increment' => true
            ],
            'fullname' => [
                'type' => 'VARCHAR',
                'constraint' => 50
            ],
            'username' => [
                'type' => 'VARCHAR',
                'constraint' => 50
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 100
            ],
            'password' => [
                'type' => 'VARCHAR',
                'constraint' => 200
            ],
            'sex' => [
                'type' => 'VARCHAR',
                'constraint' => 10
            ],
            'address' => [
                'type' => 'VARCHAR',
                'constraint' => 200
            ],
            'whatsno' => [
                'type' => 'VARCHAR',
                'constraint' => 15
            ],
            'role' => [
                'type' => 'VARCHAR',
                'constraint' => 10
            ]
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('users');
    }
    public function down()
    {
        //
    }
}
