<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;
use App\Models\UserModel;

class Action extends ResourceController
{
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    use ResponseTrait;
    function __construct() {
        $this->model = new UserModel();
    }

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    {
        $data = $this->model->orderBy('name', 'asc')->findAll();
        return $this->respond($data, 200);
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        $data = $this->model->where('id', $id)->first();
        if($data) {
            return $this->respondCreated($data, 201);
        } else {
            return $this->failNotFound("User $id doesn't exist");
        }
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        //
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        // $data = [
        //     'nama' => $this->request->getVar('nama'),
        //     'username' => $this->request->getVar('username'),
        //     'email' => $this->request->getVar('email'),
        //     'password' => $this->request->getVar('password'),
        //     'alamat' => $this->request->getVar('alamat'),
        //     'nohp' => $this->request->getVar('nohp'),
        //     'role' => $this->request->getVar('role')
        // ];
        // if(!$this->model->save($data)) {
        //     return $this->fail($this->model->errors());
        // }
        // $response = [
        //     'status' => 201,
        //     'error' => null,
        //     'messages' => [
        //         'success' => 'User successfully created'
        //     ]
        //     ];
        //     return $this->respond($response);
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        $model = new UserModel();
        $isExist = $this->model->where('id', $id)->findAll();
        if(!$isExist) {
            return $this->failNotFound("User $id doesn't exist");
        } else {
            helper(['form']);
            $rules = [
                'nama' => 'required|min_length[3]|alpha_space',
                'alamat' => 'required',
                'nohp' => 'required',
                'email' => 'required|valid_email|is_unique[users.email]',
            ];
            if (!$this->validate($rules)) return $this->fail($this->validator->getErrors());
            $data = [
            'nama' => $this->request->getVar('nama'),
            'email' => $this->request->getVar('email'),
            'alamat' => $this->request->getVar('alamat'),
            'nohp' => $this->request->getVar('nohp'),
            'role' => $this->request->getVar('role'),
            ];
        }
        $model->update($id, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'messages' => [
                'success' => "User $id successfully updated"
            ]
        ];
        return $this->respond($response);
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        $data = $this->model->where('id', $id)->findAll();
        if($data) {
            $this->model->delete($id);
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => [
                    'success' => "User $id successfully deleted"
                ]
            ];
            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound("User $id doesn't exist");
        }
    }
}