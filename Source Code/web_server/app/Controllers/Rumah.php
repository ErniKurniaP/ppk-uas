<?php

namespace App\Models;

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\RumahModel;

class Rumah extends ResourceController
{
    use ResponseTrait;
    function __construct() {
        $this->model = new RumahModel();
    }

    // all users
    public function index()
    {
        $model = new RumahModel();
        $data['rumah'] = $model->orderBy('id', 'DESC')->findAll();
        $response = [
            'status'   => 201,
            'rumah'    => $data['rumah'],
            'messages' => [
                'success' => 'Data rumah berhasil ditampilkan.'
            ]
        ];


        return $this->respond($response);
    }

    // create
    public function create()
    {
        helper(['form']);
        $rules = [
            'nama' => 'required|min_length[3]',
            'alamat' => 'required',
            'jenis' => 'required',
            'tahunBerdiri' => 'required|max_length[4]',
            'harga' => 'required',
            'namaPemilik' => 'required|min_length[3]|alpha_space',
            'narahubung' => 'required'
        ];
        if (!$this->validate($rules)) return $this->fail($this->validator->getErrors());
        $data = [
            'nama' => $this->request->getVar('nama'),
            'alamat' => $this->request->getVar('alamat'),
            'jenis' => $this->request->getVar('jenis'),
            'tahunBerdiri' => $this->request->getVar('tahunBerdiri'),
            'harga' => $this->request->getVar('harga'),
            'namaPemilik' => $this->request->getVar('namaPemilik'),
            'narahubung' => $this->request->getVar('narahubung')
        ];
        $model = new RumahModel();
        $model->save($data);
        $response = [
            'status'   => 201,
            'error'    => null,
            'messages' => [
                'success' => 'Rumah berhasil ditambahkan.'
            ]
        ];
        return $this->respondCreated($response);
    }

    // single user
    public function show($id = null)
    {
        $model = new RumahModel();
        $data = $model->where('id', $id)->first();
        if ($data) {
            return $this->respond($data);
        } else {
            return $this->failNotFound('Rumah tidak ditemukan.');
        }
    }

    // update
        /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        $model = new RumahModel();
        $isExist = $this->model->where('id', $id)->findAll();
        if(!$isExist) {
            return $this->failNotFound("Rumah $id tidak ada dalam data!");
        } else {
            helper(['form']);
            $rules = [
                'nama' => 'required|min_length[3]',
                'alamat' => 'required',
                'jenis' => 'required',
                'tahunBerdiri' => 'required|max_length[4]',
                'harga' => 'required',
                'namaPemilik' => 'required|min_length[3]|alpha_space',
                'narahubung' => 'required'
            ];
            if (!$this->validate($rules)) return $this->fail($this->validator->getErrors());
            $data = [
                'nama' => $this->request->getVar('nama'),
                'alamat' => $this->request->getVar('alamat'),
                'jenis' => $this->request->getVar('jenis'),
                'tahunBerdiri' => $this->request->getVar('tahunBerdiri'),
                'harga' => $this->request->getVar('harga'),
                'namaPemilik' => $this->request->getVar('namaPemilik'),
                'narahubung' => $this->request->getVar('narahubung'),
            ];
        }
        $model->update($id, $data);
        $response = [
            'status'   => 201,
            'error'    => null,
            'messages' => [
                'success' => 'Rumah berhasil diubah.'
            ]
        ];
        return $this->respond($response);
    }


    // delete
    public function delete($id = null)
    {
        $data = $this->model->where('id', $id)->findAll();
        if($data) {
            $this->model->delete($id);
            $response = [
                'status' => 201,
                'error' => null,
                'messages' => [
                    'success' => "Rumah $id berhasil dihapus"
                ]
            ];
            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound("Rumah $id tidak ada dalam data");
        }
    }
}
