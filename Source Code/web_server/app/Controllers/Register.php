<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;

class Register extends ResourceController
{
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    use ResponseTrait;
    public function index()
    {
        helper(['form']);
        $rules = [
            'nama' => 'required|min_length[3]|alpha_space',
            'username' => 'required|min_length[3]|is_unique[users.username]',
            'alamat' => 'required',
            'nohp' => 'required',
            'email' => 'required|valid_email|is_unique[users.email]',
            'password' => 'required|max_length[15]|min_length[6]',
            'confpassword' => 'matches[password]',
            'role' => 'required'
        ];
        if (!$this->validate($rules)) return $this->fail($this->validator->getErrors());
        $data = [
            'nama' => $this->request->getVar('nama'),
            'email' => $this->request->getVar('email'),
            'alamat' => $this->request->getVar('alamat'),
            'nohp' => $this->request->getVar('nohp'),
            'username' => $this->request->getVar('username'),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
            'role' => $this->request->getVar('role'),
        ];
        $model = new UserModel();
        $model->save($data);
        $response = [
            'status' => 201,
            'error' => null,
            'messages' => [
                'success' => 'Akun berhasil dibuat!'
            ]
            ];
        return $this->respond($response);
    }
}
