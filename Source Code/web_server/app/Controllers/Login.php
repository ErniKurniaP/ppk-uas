<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;
use Firebase\JWT\JWT;
use Firebase\JWT\Key; 

class Login extends ResourceController
{
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    use ResponseTrait;
    public function index()
    {
        helper(['form']);
        $rules = [
            'username' => 'required|min_length[3]|alpha_numeric',
            'password' => 'required|min_length[6]'
        ];
        if (!$this->validate($rules)) return $this->fail($this->validator->getErrors());
        $model = new UserModel();
        $user = $model->where("username", $this->request->getVar('username'))->first();
        if (!$user) return $this->failNotFound('Username tidak ditemukan!');
        $verify = password_verify($this->request->getVar('password'), $user['password']);
        if (!$verify) return $this->fail('Password salah!');
        $key = getenv('TOKEN_SECRET');
        $payload = array(
            "iat" => 1356999524,
            "nbf" => 1357000000,
            "uid" => $user['id'],
            "nama" => $user['nama'],
            "username" => $user['username'],
            "password" => $user['password'],
            "email" => $user['email'],
            "alamat" => $user['alamat'],
            "nohp" => $user['nohp'],
            "role" => $user['role']
        );
        $token = JWT::encode($payload, $key, 'HS256');
        JWT::decode($token, new Key($key, 'HS256'));
        $response = [
            'status' => 201,
            'error' => null,
            'id' => $user['id'],
            'token' => $token,
            'nama' => $user['nama'],
            'username' => $user['username'],
            "password" => $user['password'],
            'email' => $user['email'],
            'alamat' => $user['alamat'],
            'nohp' => $user['nohp'],
            'role' => $user['role'],
            'messages' => [
                'success' => 'Berhasil login!'
            ]
            ];
        return $this->respond($response);
    }
}
