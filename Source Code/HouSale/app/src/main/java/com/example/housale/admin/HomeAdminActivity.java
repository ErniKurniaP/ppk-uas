package com.example.housale.admin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.housale.LoginActivity;
import com.example.housale.R;
import com.example.housale.session.PrefSetting;
import com.example.housale.session.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

public class HomeAdminActivity extends AppCompatActivity {

    private CardView listRumah, tambahRumah, profile;
    private Button btnLogout;
    TextView titleNama;
    String id, nama, password, username, alamat, email, nohp, role;
    SessionManager session;
    SharedPreferences prefs;
    PrefSetting preferenceSetting;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_admin);

        getSupportActionBar().hide();

        titleNama = findViewById(R.id.titleNama);
        titleNama.setText(PrefSetting.username);
        id = PrefSetting.id;

        preferenceSetting = new PrefSetting(this);
        prefs = preferenceSetting.getSharePreferences();
        session = new SessionManager(HomeAdminActivity.this);

        listRumah = findViewById(R.id.listRumah);
        tambahRumah = findViewById(R.id.tambahRumah);
        profile = findViewById(R.id.profile);
        btnLogout = findViewById(R.id.btnlogout);

        requestQueue = Volley.newRequestQueue(this);
        preferenceSetting.isLogin(session, prefs);

        listRumah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listRumah = new Intent(HomeAdminActivity.this, ListRumahActivity.class);
                startActivity(listRumah);
            }
        });
        tambahRumah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tambahRumah = new Intent(HomeAdminActivity.this, InputRumahActivity.class);
                startActivity(tambahRumah);
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProfile();
//                Intent profile = new Intent(HomeAdminActivity.this, AdminProfileActivity.class);
//                startActivity(profile);
            }
        });
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setLogin(false);
                session.setSessid(0);
                Toast.makeText(HomeAdminActivity.this, "Berhasil logout!", Toast.LENGTH_LONG).show();

                Intent logout = new Intent(HomeAdminActivity.this, LoginActivity.class);
                startActivity(logout);
            }
        });
    }

    public void getProfile() {
                JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, "http://192.168.56.1/web_server/public/show/" + id, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("Response API = ", response.toString());
//                            String status = response.getString("status");
//                            if (status.equals("201")) {
//                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                            nama = response.getString("nama");
                            email = response.getString("email");
                            alamat = response.getString("alamat");
                            nohp = response.getString("nohp");
                            password = response.getString("password");
                            username = response.getString("username");
                            role = response.getString("role");

                            Intent a = new Intent(HomeAdminActivity.this, AdminProfileActivity.class);
                            a.putExtra("nama", nama);
                            a.putExtra("email", email);
                            a.putExtra("alamat", alamat);
                            a.putExtra("nohp", nohp);
                            a.putExtra("username", username);
                            a.putExtra("password", password);
                            a.putExtra("role", role);
                            a.putExtra("id", id);
                            startActivity(a);
//                                        }
//                                    });
//
//                                    newList.add(rumahData);
//                                }
//                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });

        /* Add your Requests to the RequestQueue to execute */
        requestQueue.add(req);
    }
}