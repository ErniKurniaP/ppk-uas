package com.example.housale.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.housale.R;
import com.example.housale.adapter.RumahAdapter;
import com.example.housale.model.RumahModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListRumahActivity extends AppCompatActivity {

    RumahAdapter rumahAdapter;
    ListView listView;
    ImageView back;

    ArrayList<RumahModel> newList = new ArrayList<RumahModel>();
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_rumah);

        getSupportActionBar().setTitle("List Rumah");
       requestQueue = Volley.newRequestQueue(this);

        listView = (ListView) findViewById(R.id.array_list);
        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back = new Intent(ListRumahActivity.this, HomeAdminActivity.class);
                startActivity(back);
            }
        });

        newList.clear();
        rumahAdapter = new RumahAdapter(ListRumahActivity.this, newList);
        listView.setAdapter(rumahAdapter);
        getAllRumah();
    }

    private void getAllRumah() {
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, "http://192.168.56.1/web_server/public/getall-rumah", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String rumah = response.getString("rumah");
                            String messages = response.getString("messages");
                            JSONArray jsonArray = new JSONArray(rumah);
                            if (status.equals("201")) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    final RumahModel rumahData = new RumahModel();
                                    final String nama = jsonObject.getString("nama");
                                    final String jenis = jsonObject.getString("jenis");
                                    final String alamat = jsonObject.getString("alamat");
                                    final String harga = jsonObject.getString("harga");
                                    final String tahunBerdiri = jsonObject.getString("tahunBerdiri");
                                    final String namaPemilik = jsonObject.getString("namaPemilik");
                                    final String narahubung = jsonObject.getString("narahubung");
                                    final String id = jsonObject.getString("id");
                                    rumahData.setNama(nama);
                                    rumahData.setAlamat(alamat);
                                    rumahData.setJenis(jenis);
                                    rumahData.setHarga(harga);
                                    rumahData.setTahunBerdiri(tahunBerdiri);
                                    rumahData.setNamaPemilik(namaPemilik);
                                    rumahData.setNarahubung(narahubung);
                                    rumahData.setID(id);

//                                    Toast.makeText(ListRumahActivity.this, "Berhasil menampilkan list rumah!", Toast.LENGTH_LONG).show();

                                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            // TODO Auto-generated method stub
                                            Intent a = new Intent(ListRumahActivity.this, DetailRumahActivity.class);
                                            a.putExtra("nama", newList.get(position).getNama());
                                            a.putExtra("jenis", newList.get(position).getJenis());
                                            a.putExtra("alamat", newList.get(position).getAlamat());
                                            a.putExtra("harga", newList.get(position).getHarga());
                                            a.putExtra("tahunBerdiri", newList.get(position).getTahunBerdiri());
                                            a.putExtra("namaPemilik", newList.get(position).getNamaPemilik());
                                            a.putExtra("narahubung", newList.get(position).getNarahubung());
                                            a.putExtra("id", newList.get(position).getID());
                                            startActivity(a);
                                        }
                                    });
                                    newList.add(rumahData);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        rumahAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });

        /* Add your Requests to the RequestQueue to execute */
        requestQueue.add(req);
    }

}