package com.example.housale.adapter;

import android.app.Activity;
import android.content.Context;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.housale.R;
import com.example.housale.model.RumahModel;
import com.example.housale.server.BaseURL;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RumahAdapter extends BaseAdapter {

    ImageView gambarRumah, back;
    TextView nama, jenis, alamat, tahunBerdiri, harga, namaPemilik;
    private Activity activity;
    private LayoutInflater inflater;
    List<RumahModel> item;

    public RumahAdapter(Activity activity, List<RumahModel> item) {
        this.activity = activity;
        this.item = item;
    }

    @Override
    public int getCount() {
        return item.size();
    }

    @Override
    public Object getItem(int position) {
        return item.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null)
            convertView = inflater.inflate(R.layout.list_rumah, null);

        nama = (TextView) convertView.findViewById(R.id.namaRumah);
        jenis = (TextView) convertView.findViewById(R.id.jenisRumah);
        alamat = (TextView)  convertView.findViewById(R.id.alamatRumah);
        harga = (TextView) convertView.findViewById(R.id.hargaRumah);
        tahunBerdiri = (TextView) convertView.findViewById(R.id.tahunBerdiri);
        gambarRumah = (ImageView) convertView.findViewById(R.id.gambarRumah);
        back = (ImageView) convertView.findViewById(R.id.back);

        nama.setText(item.get(position).getNama());
        jenis.setText(item.get(position).getJenis());
        alamat.setText(item.get(position).getAlamat());
        harga.setText(item.get(position).getHarga());
        tahunBerdiri.setText(item.get(position).getTahunBerdiri());
//        Picasso.get().load(BaseURL.baseUrl + "gambar/" + item.get(position).getGambar())
//                .resize(40,40)
//                .centerCrop()
//                .into(gambarRumah);

        return convertView;
    }
}
