package com.example.housale.session;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.housale.admin.HomeAdminActivity;

public class PrefSetting {

    public static String id, token, nama, email, alamat, nohp, username, password, role;
    Activity activity;

    public PrefSetting(Activity activity) {
        this.activity = activity;
    }

    public SharedPreferences getSharePreferences() {
        SharedPreferences preferences = activity.getSharedPreferences("DetailPengguna",Context.MODE_PRIVATE);
        return preferences;
    }

    public void isLogin(SessionManager session, SharedPreferences pref) {
        session = new SessionManager(activity);
        if(session.isLoggedIn()) {
            pref = getSharePreferences();
            id = pref.getString("id","");
            token = pref.getString("token","");
            nama = pref.getString("nama","");
            email = pref.getString("email","");
            alamat = pref.getString("alamat","");
            nohp = pref.getString("nohp","");
            username = pref.getString("username","");
            password = pref.getString("password", "");
            role = pref.getString("role","");
        } else {
            session.setLogin(false);
            session.setSessid(0);
            Intent i = new Intent(activity, activity.getClass());
            activity.startActivity(i);
        }
    }

    public  void checkLogin(SessionManager session, SharedPreferences pref) {
        session = new SessionManager(activity);
        id = pref.getString("id","");
        token = pref.getString("token","");
        nama = pref.getString("nama","");
        email = pref.getString("email","");
        alamat = pref.getString("alamat","");
        nohp = pref.getString("nohp","");
        username = pref.getString("username","");
        password = pref.getString("password", "");
        role = pref.getString("role","");
        if(session.isLoggedIn()) {
            if(role.equals("admin")) {
                Intent i = new Intent(activity, HomeAdminActivity.class);
                activity.startActivity(i);
            } else {
//                Intent i = new Intent(activity, HomeBuyerActivity.class);
//                activity.startActivity(i);
            }
        }
    }

    public void storeRegTokenSharedPreferences(Context context, String id, String token,
                                               String nama, String email, String alamat, String nohp, String username, String password,
                                               String role, SharedPreferences prefs) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("id",id);
        editor.putString("token", token);
        editor.putString("nama", nama);
        editor.putString("email",email);
        editor.putString("alamat", alamat);
        editor.putString("username", username);
        editor.putString("password", password);
        editor.putString("nohp",nohp);
        editor.putString("role",role);
        editor.commit();
    }
}

