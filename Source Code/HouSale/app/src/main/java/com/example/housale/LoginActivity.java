package com.example.housale;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.housale.admin.HomeAdminActivity;
import com.example.housale.buyer.HomeMemberActivity;
import com.example.housale.session.PrefSetting;
import com.example.housale.session.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private ImageView back;
    private Button btnLogin;
    private EditText usrField, pwdField;
    private TextView register;

    private RequestQueue requestQueue;

    SessionManager session;
    SharedPreferences prefs;
    PrefSetting prefSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().hide();
        requestQueue = Volley.newRequestQueue(this);

        back = findViewById(R.id.back);
        usrField = findViewById(R.id.usrField);
        pwdField = findViewById(R.id.pwdField);
        btnLogin = findViewById(R.id.btnLogin);
        register = findViewById(R.id.register);

        prefSetting = new PrefSetting(this);
        prefs = prefSetting.getSharePreferences();
        session = new SessionManager(this);
        prefSetting.checkLogin(session, prefs);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(back);
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent register = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(register);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = usrField.getText().toString().trim();
                String password = pwdField.getText().toString().trim();
                String role = "member";

                if(!userName.isEmpty() && !password.isEmpty()) {
                    Login(userName, password);
                } else {
                    usrField.setError("Username harus diisi!");
                    pwdField.setError("Password harus diisi!");
                }
            }
        });
    }

    public void Login(final String userName, final String password) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://192.168.56.1/web_server/public/login", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String id = jsonObject.getString("id");
                    String token = jsonObject.getString("token");
                    String nama = jsonObject.getString("nama");
                    String email = jsonObject.getString("email");
                    String alamat = jsonObject.getString("alamat");
                    String nohp = jsonObject.getString("nohp");
                    String username = jsonObject.getString("username");
                    String password = jsonObject.getString("password");
                    String role = jsonObject.getString("role");
                    String messages = jsonObject.getString("messages");
                    if (status.equals("201")) {

                        session.setLogin(true);
                        prefSetting.storeRegTokenSharedPreferences(LoginActivity.this, id, token, nama, email, alamat, nohp, username, password, role, prefs);

                        if (role.equals("member")) {
                            Toast.makeText(LoginActivity.this, "Berhasil login sebagai member!", Toast.LENGTH_LONG).show();

                            Intent success = new Intent(LoginActivity.this, HomeMemberActivity.class);
                            startActivity(success);
                        } else {
                            Toast.makeText(LoginActivity.this, "Berhasil login sebagai admin!", Toast.LENGTH_LONG).show();

                            Intent success = new Intent(LoginActivity.this, HomeAdminActivity.class);
                            startActivity(success);
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, messages, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Login Error!", Toast.LENGTH_LONG).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, "Terdapat kesalahan pada username atau password!", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                // Post params to be sent to the server
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", userName);
                params.put("password", password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}