package com.example.housale.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.housale.LoginActivity;
import com.example.housale.R;
import com.example.housale.RegisterActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class InputRumahActivity extends AppCompatActivity {

    TextView namaField, jenisField, alamatField, hargaField, tahunBerdiriField, namaPemilikField, narahubungField;
    Button btnSimpan;
    ImageView back;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_rumah);

        getSupportActionBar().hide();

        requestQueue = Volley.newRequestQueue(this);

        namaField = findViewById(R.id.namaField);
        jenisField = findViewById(R.id.jenisField);
        alamatField = findViewById(R.id.alamatField);
        hargaField = findViewById(R.id.hargaField);
        tahunBerdiriField = findViewById(R.id.tahunBerdiriField);
        namaPemilikField = findViewById(R.id.namaPemilikField);
        narahubungField = findViewById(R.id.narahubungField);
        back = findViewById(R.id.back);
        btnSimpan = findViewById(R.id.btnSimpan);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back = new Intent(InputRumahActivity.this, HomeAdminActivity.class);
                startActivity(back);
            }
        });
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputRumah();
            }
        });
    }

    public  void inputRumah() {
        final String nama = this.namaField.getText().toString().trim();
        final String jenis = this.jenisField.getText().toString().trim();
        final String alamat = this.alamatField.getText().toString().trim();
        final String harga = this.hargaField.getText().toString().trim();
        final String tahunBerdiri = this.tahunBerdiriField.getText().toString().trim();
        final String namaPemilik = this.namaPemilikField.getText().toString().trim();
        final String narahubung = this.narahubungField.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://192.168.56.1/web_server/public/create-rumah", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String messages = jsonObject.getString("messages");
                    if (status.equals("201")) {
                        Toast.makeText(InputRumahActivity.this, nama + " berhasil ditambahkan!", Toast.LENGTH_LONG).show();
                        Intent success = new Intent(InputRumahActivity.this, ListRumahActivity.class);
                        startActivity(success);
                    } else {
                        Toast.makeText(InputRumahActivity.this, messages, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(InputRumahActivity.this, "Registrasi Error!", Toast.LENGTH_LONG).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(InputRumahActivity.this, "Registrasi Error!", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                // Post params to be sent to the server
                Map<String, String> params = new HashMap<String, String>();
                params.put("nama", nama);
                params.put("jenis", jenis);
                params.put("harga", harga);
                params.put("alamat", alamat);
                params.put("tahunBerdiri", tahunBerdiri);
                params.put("namaPemilik", namaPemilik);
                params.put("narahubung", narahubung);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}