package com.example.housale.admin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.housale.MainActivity;
import com.example.housale.R;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailRumahActivity extends AppCompatActivity {

    TextView namaField, jenisField, alamatField, tahunBerdiriField, hargaField, namaPemilikField, narahubungField;
    Button btnEdit, btnHapus;
    ImageView back;
    String id, nama, jenis, alamat, harga, tahunBerdiri, namaPemilik, narahubung;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_rumah);

        getSupportActionBar().setTitle("Detail Rumah");

        namaField = findViewById(R.id.namaField);
        jenisField = findViewById(R.id.jenisField);
        hargaField = findViewById(R.id.hargaField);
        alamatField = findViewById(R.id.alamatField);
        tahunBerdiriField = findViewById(R.id.tahunBerdiriField);
        namaPemilikField = findViewById(R.id.namaPemilikField);
        narahubungField = findViewById(R.id.narahubungField);
        back = findViewById(R.id.back);
        btnEdit = findViewById(R.id.btnEdit);
        btnHapus = findViewById(R.id.btnHapus);

        requestQueue = Volley.newRequestQueue(this);

        Intent i = getIntent();
        id = i.getStringExtra("id");
        nama = i.getStringExtra("nama");
        alamat = i.getStringExtra("alamat");
        jenis = i.getStringExtra("jenis");
        harga = i.getStringExtra("harga");
        namaPemilik = i.getStringExtra("namaPemilik");
        narahubung = i.getStringExtra("narahubung");
        tahunBerdiri = i.getStringExtra("tahunBerdiri");

        namaField.setText(nama);
        alamatField.setText(alamat);
        hargaField.setText(harga);
        jenisField.setText(jenis);
        namaPemilikField.setText(namaPemilik);
        narahubungField.setText(narahubung);
        tahunBerdiriField.setText(tahunBerdiri);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back = new Intent(DetailRumahActivity.this, ListRumahActivity.class);
                startActivity(back);
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(DetailRumahActivity.this, EditRumahActivity.class);
                // TODO Auto-generated method stub
                a.putExtra("nama", nama);
                a.putExtra("jenis", jenis);
                a.putExtra("alamat", alamat);
                a.putExtra("harga", harga);
                a.putExtra("tahunBerdiri", tahunBerdiri);
                a.putExtra("namaPemilik", namaPemilik);
                a.putExtra("narahubung", narahubung);
                a.putExtra("id", id);
                startActivity(a);
            }
        });

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DetailRumahActivity.this);

                builder.setTitle("Konfirmasi");
                builder.setMessage("Apakah Anda yakin ingin menghapus " + nama + " ? ");

                builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        hapusData();
                    }
                });
                builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    public void hapusData() {
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.DELETE,"http://192.168.56.1/web_server/public/delete-rumah/" + id, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String messages = response.getString("messages");
                            String status = response.getString("status");
                            if(status.equals("201")) {
                                Toast.makeText(getApplicationContext(), nama + " berhasil dihapus!", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(DetailRumahActivity.this, ListRumahActivity.class);
                                startActivity(i);
                            } else {
                                Toast.makeText(getApplicationContext(), messages, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });
        requestQueue.add(req);
    }
}