package com.example.housale.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.housale.R;
import com.example.housale.model.RumahModel;
import com.example.housale.session.PrefSetting;
import com.example.housale.session.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdminProfileActivity extends AppCompatActivity {

    TextView usrField, namaField, emailField, alamatField, noHpField, changePassword;
    ImageView back;
    Button btnEdit;
    String id, nama, email, nohp, alamat, username, password, role;
    private RequestQueue requestQueue;

    SessionManager session;
    SharedPreferences prefs;
    PrefSetting preferenceSetting;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getSupportActionBar().setTitle("Profile");

        back = findViewById(R.id.back);
        usrField = findViewById(R.id.usrField);
        namaField = findViewById(R.id.namaField);
        alamatField = findViewById(R.id.alamatField);
        emailField = findViewById(R.id.emailField);
        noHpField = findViewById(R.id.nohpField);
        btnEdit = findViewById(R.id.btnEdit);
        changePassword = findViewById(R.id.changePassword);

        requestQueue = Volley.newRequestQueue(this);

        Intent i = getIntent();
        id = i.getStringExtra("id");
        nama = i.getStringExtra("nama");
        alamat = i.getStringExtra("alamat");
        email = i.getStringExtra("email");
        username = i.getStringExtra("username");
        nohp = i.getStringExtra("nohp");
        role = i.getStringExtra("role");

        usrField.setText(username);
        namaField.setText(nama);
        emailField.setText(email);
        alamatField.setText(alamat);
        noHpField.setText(nohp);

        //getProfile();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back = new Intent(AdminProfileActivity.this, HomeAdminActivity.class);
                startActivity(back);
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(AdminProfileActivity.this, EditAdminProfileActivity.class);
                a.putExtra("nama", nama);
                a.putExtra("email", email);
                a.putExtra("alamat", alamat);
                a.putExtra("nohp", nohp);
                a.putExtra("username", username);
                a.putExtra("password", password);
                a.putExtra("role", role);
                a.putExtra("id", id);
                startActivity(a);
            }
        });
    }

//    public void getProfile() {
//        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, "http://192.168.56.1/web_server/public/show/" + id, null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            Log.d("Response API = ", response.toString());
//                            String status = response.getString("status");
////                            if (status.equals("201")) {
////                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
//                            String namaa = response.getString("nama");
//                            String emaill = response.getString("email");
//                            String alamatt = response.getString("alamat");
//                            String nohpp = response.getString("nohp");
//                            String passwordd = response.getString("password");
//                            String usernamee = response.getString("username");
//                            String idd = response.getString("id");
//                            usrField.setText(usernamee);
//                            namaField.setText(namaa);
//                            emailField.setText(emaill);
//                            alamatField.setText(alamatt);
//                            noHpField.setText(nohpp);
//
//                            Toast.makeText(AdminProfileActivity.this, namaa, Toast.LENGTH_LONG).show();
//
////                                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
////                                        @Override
////                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                                            // TODO Auto-generated method stub
////                                            Toast.makeText(ListRumahActivity.this, "Berhasil klik", Toast.LENGTH_LONG).show();
//                            Intent a = new Intent(AdminProfileActivity.this, AdminProfileActivity.class);
//                            a.putExtra("nama", namaa);
//                            a.putExtra("email", emaill);
//                            a.putExtra("alamat", alamatt);
//                            a.putExtra("nohp", nohpp);
//                            a.putExtra("username", usernamee);
//                            a.putExtra("password", passwordd);
//                            a.putExtra("id", idd);
//                            startActivity(a);
////                                        }
////                                    });
////
////                                    newList.add(rumahData);
////                                }
////                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.e("Error: ", error.getMessage());
//            }
//        });
//
//        /* Add your Requests to the RequestQueue to execute */
//        requestQueue.add(req);
//    }

    }