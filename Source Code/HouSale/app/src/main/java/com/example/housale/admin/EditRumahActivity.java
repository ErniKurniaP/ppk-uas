package com.example.housale.admin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.housale.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class EditRumahActivity extends AppCompatActivity {

    TextView namaField, jenisField, alamatField, hargaField, tahunBerdiriField, namaPemilikField, narahubungField;
    Button btnEdit;
    ImageView back;
    String id, nama, jenis, alamat, tahunBerdiri, harga, namaPemilik, narahubung;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_rumah);

        getSupportActionBar().setTitle("Edit Data Rumah");

        namaField = findViewById(R.id.namaField);
        jenisField = findViewById(R.id.jenisField);
        alamatField = findViewById(R.id.alamatField);
        tahunBerdiriField = findViewById(R.id.tahunBerdiriField);
        hargaField = findViewById(R.id.hargaField);
        namaPemilikField = findViewById(R.id.namaPemilikField);
        narahubungField = findViewById(R.id.narahubungField);
        btnEdit = findViewById(R.id.btnEdit);
        back = findViewById(R.id.back);

        requestQueue = Volley.newRequestQueue(this);

        Intent i = getIntent();
        id = i.getStringExtra("id");
        nama = i.getStringExtra("nama");
        alamat = i.getStringExtra("alamat");
        jenis = i.getStringExtra("jenis");
        harga = i.getStringExtra("harga");
        namaPemilik = i.getStringExtra("namaPemilik");
        narahubung = i.getStringExtra("narahubung");
        tahunBerdiri = i.getStringExtra("tahunBerdiri");

        namaField.setText(nama);
        alamatField.setText(alamat);
        jenisField.setText(jenis);
        namaPemilikField.setText(namaPemilik);
        narahubungField.setText(narahubung);
        hargaField.setText(harga);
        tahunBerdiriField.setText(tahunBerdiri);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditRumahActivity.this);

                builder.setTitle("Perhatian");
                builder.setMessage("Perubahan yang Anda lakukan belum tersimpan!");

                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        Intent a = new Intent(EditRumahActivity.this, DetailRumahActivity.class);
                        // TODO Auto-generated method stub
                        a.putExtra("nama", nama);
                        a.putExtra("jenis", jenis);
                        a.putExtra("alamat", alamat);
                        a.putExtra("harga", harga);
                        a.putExtra("tahunBerdiri", tahunBerdiri);
                        a.putExtra("namaPemilik", namaPemilik);
                        a.putExtra("narahubung", narahubung);
                        a.putExtra("id", id);
                        startActivity(a);
                    }
                });
                builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editData();
            }
        });
    }

    public  void editData() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("nama", namaField.getText().toString());
        params.put("jenis", jenisField.getText().toString());
        params.put("alamat", alamatField.getText().toString());
        params.put("tahunBerdiri", tahunBerdiriField.getText().toString());
        params.put("harga", hargaField.getText().toString());
        params.put("namaPemilik", namaPemilikField.getText().toString());
        params.put("narahubung", narahubungField.getText().toString());

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT,"http://192.168.56.1/web_server/public/update-rumah/" + id, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String messages = response.getString("messages");
                            String status = response.getString("status");
                            if(status.equals("201")){
                                Toast.makeText(getApplicationContext(), namaField.getText().toString() + " berhasil diedit!", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(EditRumahActivity.this, DetailRumahActivity.class);
                                // TODO Auto-generated method stub
                                i.putExtra("id", id);
                                i.putExtra("nama", namaField.getText().toString());
                                i.putExtra("jenis", jenisField.getText().toString());
                                i.putExtra("harga", hargaField.getText().toString());
                                i.putExtra("tahunBerdiri", tahunBerdiriField.getText().toString());
                                i.putExtra("alamat", alamatField.getText().toString());
                                i.putExtra("namaPemilik", namaPemilikField.getText().toString());
                                i.putExtra("narahubung", narahubungField.getText().toString());
                                startActivity(i);
                            }else {
                                Toast.makeText(getApplicationContext(), messages, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });
        requestQueue.add(req);
    }
}