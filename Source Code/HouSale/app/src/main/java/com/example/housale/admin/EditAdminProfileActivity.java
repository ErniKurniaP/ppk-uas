package com.example.housale.admin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.housale.R;
import com.example.housale.session.PrefSetting;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class EditAdminProfileActivity extends AppCompatActivity {

    TextView namaField, alamatField, emailField, noHpField;
    Button btnEdit;
    ImageView back;
    String id, token, nama, username, alamat, email, nohp, role;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_admin_profile);

        getSupportActionBar().setTitle("Edit Profile");

        namaField = findViewById(R.id.namaField);
        alamatField = findViewById(R.id.alamatField);
        emailField = findViewById(R.id.emailField);
        noHpField = findViewById(R.id.nohpField);
        btnEdit = findViewById(R.id.btnEdit);
        back = findViewById(R.id.back);

        requestQueue = Volley.newRequestQueue(this);

        Intent i = getIntent();
        id = i.getStringExtra("id");
        nama = i.getStringExtra("nama");
        alamat = i.getStringExtra("alamat");
        email = i.getStringExtra("email");
        username = i.getStringExtra("username");
        nohp = i.getStringExtra("nohp");
        role = i.getStringExtra("role");


        namaField.setText(nama);
        emailField.setText(email);
        alamatField.setText(alamat);
        noHpField.setText(nohp);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditAdminProfileActivity.this);

                builder.setTitle("Perhatian");
                builder.setMessage("Perubahan yang Anda lakukan belum tersimpan!");

                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        Intent i = new Intent(EditAdminProfileActivity.this, AdminProfileActivity.class);
                        // TODO Auto-generated method stub
                        i.putExtra("id", id);
                        i.putExtra("nama", nama);
                        i.putExtra("email", email);
                        i.putExtra("username", username);
                        i.putExtra("alamat", alamat);
                        i.putExtra("nohp", nohp);
                        i.putExtra("role", role);
                        startActivity(i);
                    }
                });
                builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editProfile();
            }
        });
    }

    public  void editProfile() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("nama", namaField.getText().toString());
        params.put("alamat", alamatField.getText().toString());
        params.put("email", emailField.getText().toString());
        params.put("nohp", noHpField.getText().toString());
        params.put("role", "admin");

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT,"http://192.168.56.1/web_server/public/update/" + id, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String messages = response.getString("messages");
                            String status = response.getString("status");
//                            if(status.equals("201")){
                                Toast.makeText(getApplicationContext(), "Profil " + namaField.getText().toString()+ " berhasil diedit!", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(EditAdminProfileActivity.this, AdminProfileActivity.class);
                            // TODO Auto-generated method stub
                            i.putExtra("id", id);
                            i.putExtra("nama", namaField.getText().toString());
                            i.putExtra("email", emailField.getText().toString());
                            i.putExtra("username", username);
                            i.putExtra("alamat", alamatField.getText().toString());
                            i.putExtra("nohp", noHpField.getText().toString());
                            i.putExtra("role", role);
                                startActivity(i);
//                            }else {
//                                Toast.makeText(getApplicationContext(), messages, Toast.LENGTH_LONG).show();
//                            }
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "Email sudah digunakan!", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.e("Error: ", error.getMessage());
                Toast.makeText(getApplicationContext(), "Email sudah digunakan!", Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(req);
    }

}