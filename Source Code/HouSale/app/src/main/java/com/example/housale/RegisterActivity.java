package com.example.housale;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.housale.server.BaseURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    private TextView login;
    private ImageView back;
    private Button btnRegister;
    private EditText namaField, emailField, alamatField, nohpField, usrField, pwdField, confPwdField;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getSupportActionBar().hide();
        requestQueue = Volley.newRequestQueue(this);

        back = findViewById(R.id.back);
        namaField = findViewById(R.id.namaField);
        emailField = findViewById(R.id.emailField);
        alamatField = findViewById(R.id.alamatField);
        nohpField = findViewById(R.id.nohpField);
        usrField = findViewById(R.id.usrField);
        pwdField = findViewById(R.id.pwdField);
        confPwdField = findViewById(R.id.confPwdField);
        btnRegister = findViewById(R.id.btnRegister);
        login = findViewById(R.id.login);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(back);
            }

        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(login);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = namaField.getText().toString().trim();
                String email = emailField.getText().toString().trim();
                String alamat = alamatField.getText().toString().trim();
                String noHp = nohpField.getText().toString().trim();
                String userName = usrField.getText().toString().trim();
                String password = pwdField.getText().toString().trim();
                String confPassword = confPwdField.getText().toString().trim();
                String role = "member";

                if(!nama.isEmpty()) {
                    if (!(nama.length() > 3)) {
                        namaField.setError("Karakter harus lebih dari 3 karakter!");
                    } else if (isValidNama(nama)){
                        namaField.setError("Nama tidak valid");
                    } else {
                        if(!email.isEmpty()) {
                            if (!isValidEmail(email)) {
                                emailField.setError("Email tidak valid!");
                            } else {
                                if(!alamat.isEmpty()) {
                                    if(!noHp.isEmpty()) {
                                        if (!isValidNoHp(noHp)) {
                                            nohpField.setError("Nomor Whatsapp harus terdiri dari angka!");
                                        } else if (!(noHp.length()>9)){
                                            nohpField.setError("Nomor Whatsapp harus lebih dari 9 karakter!");
                                        } else if (!(noHp.length()<16)) {
                                            nohpField.setError("Nomor Whatsapp harus kurang dari 16 karakter!");
                                        } else {
                                            if(!userName.isEmpty()) {
                                                if (!(userName.length() > 3)) {
                                                    usrField.setError("Username harus lebih dari 3 karakter!");
                                                } else if (isValidUsername(userName)){
                                                    usrField.setError("Username tidak valid");
                                                } else {
                                                    if(!password.isEmpty()) {
                                                        if (!(password.length() > 6)) {
                                                            pwdField.setError("Password harus lebih dari 6 karakter!");
                                                        } else if (!(password.length() < 16)){
                                                            pwdField.setError("Password harus kurang dari 16 karakter!");
                                                        } else {
                                                            if(!confPassword.isEmpty()) {
                                                                if (!(confPassword.equals(password))) {
                                                                    confPwdField.setError("Konfirmasi password tidak sesuai!");
                                                                } {
                                                                    Register();
                                                                }
                                                            } else {
                                                                confPwdField.setError("Konfirmasi password harus diisi");
                                                            }
                                                        }
                                                    } else {
                                                        pwdField.setError("Password harus diisi");
                                                    }
                                                }
                                            } else {
                                                usrField.setError("Username harus diisi");
                                            }
                                        }
                                    } else {
                                        nohpField.setError("Nomor Whatsapp harus diisi");
                                    }
                                } else {
                                    alamatField.setError("Alamat harus diisi");
                                }
                            }
                        } else {
                            emailField.setError("Email harus diisi");
                        }
                    }
                } else {
                    namaField.setError("Nama lengkap harus diisi");
                }
            }
        });
    }

    private void Register() {
        final String nama = this.namaField.getText().toString().trim();
        final String email = this.emailField.getText().toString().trim();
        final String alamat = this.alamatField.getText().toString().trim();
        final String noHp = this.nohpField.getText().toString().trim();
        final String userName = this.usrField.getText().toString().trim();
        final String password = this.pwdField.getText().toString().trim();
        final String confPassword = this.confPwdField.getText().toString().trim();
        final String role = "member";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://192.168.56.1/web_server/public/register", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String messages = jsonObject.getString("messages");
                            if (status.equals("201")) {
                                Toast.makeText(RegisterActivity.this, "Akun berhasil dibuat!", Toast.LENGTH_LONG).show();
                                Intent success = new Intent(RegisterActivity.this, LoginActivity.class);
                                startActivity(success);
                            } else {
                                Toast.makeText(RegisterActivity.this, messages, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(RegisterActivity.this, "Registrasi Error!", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterActivity.this, "Registrasi Error!", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                        // Post params to be sent to the server
                Map<String, String> params = new HashMap<String, String>();
                params.put("nama", nama);
                params.put("username", userName);
                params.put("email", email);
                params.put("alamat", alamat);
                params.put("nohp", noHp);
                params.put("password", password);
                params.put("confpassword", confPassword);
                params.put("role", role);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

public static boolean isValidEmail(String email)
{
    String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";

    Pattern pat = Pattern.compile(emailRegex);
    if (email == null)
        return false;
    return pat.matcher(email).matches();
}
    public static boolean isValidNama(String nama)
    {
        String emailRegex = "[a-zA-Z ]";

        Pattern pat = Pattern.compile(emailRegex);
        if (nama == null)
            return false;
        return pat.matcher(nama).matches();
    }
    public static boolean isValidNoHp(String nohp)
    {
        String emailRegex = "[0-9]+";

        Pattern pat = Pattern.compile(emailRegex);
        if (nohp == null)
            return false;
        return pat.matcher(nohp).matches();
    }
    public static boolean isValidUsername(String username)
    {
        String emailRegex = "[a-zA-Z0-9_-]";

        Pattern pat = Pattern.compile(emailRegex);
        if (username == null)
            return false;
        return pat.matcher(username).matches();
    }
}