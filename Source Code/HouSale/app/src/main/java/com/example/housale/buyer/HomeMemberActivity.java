package com.example.housale.buyer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.housale.R;

public class HomeMemberActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_buyer);
    }
}